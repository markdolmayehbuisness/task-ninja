import { React, useState, useEffect } from "react";
import "./signup.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import gitlab from "../../assets/gitlab.svg";

const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [error, setError] = useState(false);

  useEffect(() => {
    const keyDownHandler = (event) => {
      if (event.key === "Enter") {
        event.preventDefault();

        // 👇️ call submit function here
        signup();
      }
    };

    document.addEventListener("keydown", keyDownHandler);

    return () => {
      document.removeEventListener("keydown", keyDownHandler);
    };
  }, []);
  const login = async () => {
    axios
      .post("http://localhost:5000/api/users/login", {
        email: email,
        password: password,
      })
      .then(function (response) {
        localStorage.setItem("token", response.data.token);
        if (response) {
          navigate("/home");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const signup = async () => {
    axios
      .post("http://localhost:5000/api/users/", {
        email: email,
        password: password,
        name: username,
      })
      .then(function (response) {
        if (response) {
          login();
        }
      })
      .catch(function (error) {
        setError(true);
      });
  };

  return (
    <div className="login-container">
      <div className="login-left">
        <div className="ll-box">
          <div className="ll-box-top">
            <p style={{ marginRight: "10px" }}> - □ X</p>
          </div>
          <p className="ll-box-writing">WAKE UP NOW!!</p>
        </div>
      </div>
      <div className="login-right">
        <h1 className="lr-title">Sign Up</h1>
        {error ? (
          <h3
            style={{ marginTop: "2rem", marginBottom: "-3rem", color: "red" }}
          >
            *incorrect credentials ffs
          </h3>
        ) : (
          <></>
        )}
        <div className="login-forum">
          <div className="lr-entry">
            <h3 className="lr-entry-title">userName:</h3>
            <input
              type="text"
              className="lr-entry-text"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
          </div>
          <div className="lr-entry">
            <h3 className="lr-entry-title">Email:</h3>
            <input
              type="text"
              className="lr-entry-text"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </div>
          <div className="lr-entry">
            <h3 className="lr-entry-title">password:</h3>
            <input
              type="text"
              className="lr-entry-text"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </div>
        </div>
        <button className="lr-submit" onClick={signup}>
          Submit or whatever
        </button>
        <button
          className="lr-submit"
          style={{ marginTop: "1rem" }}
          onClick={() => {
            navigate("/login");
          }}
        >
          login
        </button>
      </div>
      <a href="https://gitlab.com/markdolmayehbuisness/task-ninja">
        <img src={gitlab} className="gitlab-logo" alt="" />
      </a>
    </div>
  );
};

export default Login;
