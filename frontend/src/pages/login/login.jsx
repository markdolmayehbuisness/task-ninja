import { useState, useEffect } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import "./login.css";
const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const login = () => {
    axios
      .post("http://localhost:5000/api/users/login", {
        email: email,
        password: password,
      })
      .then(function (response) {
        localStorage.setItem("token", response.data.token);
        if (response) {
          navigate("/home");
        }
      })
      .catch(function (error) {
        setError(true);
        console.log(error);
      });
  };

  return (
    <div className="login-container">
      <div className="login-left">
        <div className="ll-box">
          <div className="ll-box-top">
            {" "}
            <p style={{ marginRight: "10px" }}> - □ X</p>
          </div>
          <p className="ll-box-writing">WAKE UP NOW!!</p>
        </div>
      </div>
      <div className="login-right">
        <h1 className="lr-title">Login</h1>
        {error ? (
          <h3
            style={{ marginTop: "4rem", marginBottom: "-5rem", color: "red" }}
          >
            *incorrect credentials ffs
          </h3>
        ) : (
          <></>
        )}
        <div className="login-forum">
          <div className="lr-entry">
            <h3 className="lr-entry-title">Email:</h3>
            <input
              type="text"
              className="lr-entry-text"
              onKeyDown={(e) => {
                if (e.target.value == "Enter") {
                  login();
                }
              }}
              onChange={async (e) => {
                await setEmail(e.target.value);
              }}
            />
          </div>
          <div className="lr-entry">
            <h3 className="lr-entry-title">password:</h3>
            <input
              type="password"
              className="lr-entry-text"
              onChange={async (e) => {
                await setPassword(e.target.value);
              }}
            />
          </div>
        </div>
        <button className="lr-submit" onClick={login}>
          Submit or whatever
        </button>
        <button
          className="lr-submit"
          style={{ marginTop: "1rem" }}
          onClick={() => {
            navigate("/");
          }}
        >
          Sign up
        </button>
      </div>
    </div>
  );
};

export default Login;
